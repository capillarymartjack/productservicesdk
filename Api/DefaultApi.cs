/* 
 * ReadWebService
 *
 * RWS REST Endpoint
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IDefaultApi : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductInfo By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ReadWebServiceProductInfoViewV2</returns>
        ReadWebServiceProductInfoViewV2 ReadWebServiceProductServiceGetProductInfoByLocationREST (ReadWebServiceBundleProductFetchContext bulkProductFetchContext);

        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductInfo By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ApiResponse of ReadWebServiceProductInfoViewV2</returns>
        ApiResponse<ReadWebServiceProductInfoViewV2> ReadWebServiceProductServiceGetProductInfoByLocationRESTWithHttpInfo (ReadWebServiceBundleProductFetchContext bulkProductFetchContext);
        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductView By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        List<ReadWebServiceBulkProductFetchResponse> ReadWebServiceProductServiceGetProductViewsByLocationV1JSON (ReadWebServiceBulkProductFetchContext bulkProductFetchContext);

        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductView By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ApiResponse of List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        ApiResponse<List<ReadWebServiceBulkProductFetchResponse>> ReadWebServiceProductServiceGetProductViewsByLocationV1JSONWithHttpInfo (ReadWebServiceBulkProductFetchContext bulkProductFetchContext);
        #endregion Synchronous Operations
        #region Asynchronous Operations
        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductInfo By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ReadWebServiceProductInfoViewV2</returns>
        System.Threading.Tasks.Task<ReadWebServiceProductInfoViewV2> ReadWebServiceProductServiceGetProductInfoByLocationRESTAsync (ReadWebServiceBundleProductFetchContext bulkProductFetchContext);

        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductInfo By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ApiResponse (ReadWebServiceProductInfoViewV2)</returns>
        System.Threading.Tasks.Task<ApiResponse<ReadWebServiceProductInfoViewV2>> ReadWebServiceProductServiceGetProductInfoByLocationRESTAsyncWithHttpInfo (ReadWebServiceBundleProductFetchContext bulkProductFetchContext);
        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductView By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        System.Threading.Tasks.Task<List<ReadWebServiceBulkProductFetchResponse>> ReadWebServiceProductServiceGetProductViewsByLocationV1JSONAsync (ReadWebServiceBulkProductFetchContext bulkProductFetchContext);

        /// <summary>
        /// GET PRODUCT
        /// </summary>
        /// <remarks>
        /// GetProductView By Location 
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ApiResponse (List&lt;ReadWebServiceBulkProductFetchResponse&gt;)</returns>
        System.Threading.Tasks.Task<ApiResponse<List<ReadWebServiceBulkProductFetchResponse>>> ReadWebServiceProductServiceGetProductViewsByLocationV1JSONAsyncWithHttpInfo (ReadWebServiceBulkProductFetchContext bulkProductFetchContext);
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class DefaultApi : IDefaultApi
    {
        private IO.Swagger.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultApi"/> class.
        /// </summary>
        /// <returns></returns>
        public DefaultApi(String basePath)
        {
            this.Configuration = new IO.Swagger.Client.Configuration { BasePath = basePath };

            ExceptionFactory = IO.Swagger.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public DefaultApi(IO.Swagger.Client.Configuration configuration = null)
        {
            if (configuration == null) // use the default one in Configuration
                this.Configuration = IO.Swagger.Client.Configuration.Default;
            else
                this.Configuration = configuration;

            ExceptionFactory = IO.Swagger.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public String GetBasePath()
        {
            return this.Configuration.ApiClient.RestClient.BaseUrl.ToString();
        }

        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        [Obsolete("SetBasePath is deprecated, please do 'Configuration.ApiClient = new ApiClient(\"http://new-path\")' instead.")]
        public void SetBasePath(String basePath)
        {
            // do nothing
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public IO.Swagger.Client.Configuration Configuration {get; set;}

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public IO.Swagger.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Gets the default header.
        /// </summary>
        /// <returns>Dictionary of HTTP header</returns>
        [Obsolete("DefaultHeader is deprecated, please use Configuration.DefaultHeader instead.")]
        public IDictionary<String, String> DefaultHeader()
        {
            return new ReadOnlyDictionary<string, string>(this.Configuration.DefaultHeader);
        }

        /// <summary>
        /// Add default header.
        /// </summary>
        /// <param name="key">Header field name.</param>
        /// <param name="value">Header field value.</param>
        /// <returns></returns>
        [Obsolete("AddDefaultHeader is deprecated, please use Configuration.AddDefaultHeader instead.")]
        public void AddDefaultHeader(string key, string value)
        {
            this.Configuration.AddDefaultHeader(key, value);
        }

        /// <summary>
        /// GET PRODUCT GetProductInfo By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ReadWebServiceProductInfoViewV2</returns>
        public ReadWebServiceProductInfoViewV2 ReadWebServiceProductServiceGetProductInfoByLocationREST (ReadWebServiceBundleProductFetchContext bulkProductFetchContext)
        {
             ApiResponse<ReadWebServiceProductInfoViewV2> localVarResponse = ReadWebServiceProductServiceGetProductInfoByLocationRESTWithHttpInfo(bulkProductFetchContext);
             return localVarResponse.Data;
        }

        /// <summary>
        /// GET PRODUCT GetProductInfo By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ApiResponse of ReadWebServiceProductInfoViewV2</returns>
        public ApiResponse< ReadWebServiceProductInfoViewV2 > ReadWebServiceProductServiceGetProductInfoByLocationRESTWithHttpInfo (ReadWebServiceBundleProductFetchContext bulkProductFetchContext)
        {
            // verify the required parameter 'bulkProductFetchContext' is set
            if (bulkProductFetchContext == null)
                throw new ApiException(400, "Missing required parameter 'bulkProductFetchContext' when calling DefaultApi->ReadWebServiceProductServiceGetProductInfoByLocationREST");

            var localVarPath = "/GetProductInfoByLocationREST";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json"
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            if (bulkProductFetchContext != null && bulkProductFetchContext.GetType() != typeof(byte[]))
            {
                localVarPostBody = this.Configuration.ApiClient.Serialize(bulkProductFetchContext); // http body (model) parameter
            }
            else
            {
                localVarPostBody = bulkProductFetchContext; // byte array
            }


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) this.Configuration.ApiClient.CallApi(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ReadWebServiceProductServiceGetProductInfoByLocationREST", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<ReadWebServiceProductInfoViewV2>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (ReadWebServiceProductInfoViewV2) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(ReadWebServiceProductInfoViewV2)));
        }

        /// <summary>
        /// GET PRODUCT GetProductInfo By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ReadWebServiceProductInfoViewV2</returns>
        public async System.Threading.Tasks.Task<ReadWebServiceProductInfoViewV2> ReadWebServiceProductServiceGetProductInfoByLocationRESTAsync (ReadWebServiceBundleProductFetchContext bulkProductFetchContext)
        {
             ApiResponse<ReadWebServiceProductInfoViewV2> localVarResponse = await ReadWebServiceProductServiceGetProductInfoByLocationRESTAsyncWithHttpInfo(bulkProductFetchContext);
             return localVarResponse.Data;

        }

        /// <summary>
        /// GET PRODUCT GetProductInfo By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ApiResponse (ReadWebServiceProductInfoViewV2)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<ReadWebServiceProductInfoViewV2>> ReadWebServiceProductServiceGetProductInfoByLocationRESTAsyncWithHttpInfo (ReadWebServiceBundleProductFetchContext bulkProductFetchContext)
        {
            // verify the required parameter 'bulkProductFetchContext' is set
            if (bulkProductFetchContext == null)
                throw new ApiException(400, "Missing required parameter 'bulkProductFetchContext' when calling DefaultApi->ReadWebServiceProductServiceGetProductInfoByLocationREST");

            var localVarPath = "/GetProductInfoByLocationREST";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json"
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            if (bulkProductFetchContext != null && bulkProductFetchContext.GetType() != typeof(byte[]))
            {
                localVarPostBody = this.Configuration.ApiClient.Serialize(bulkProductFetchContext); // http body (model) parameter
            }
            else
            {
                localVarPostBody = bulkProductFetchContext; // byte array
            }


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await this.Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ReadWebServiceProductServiceGetProductInfoByLocationREST", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<ReadWebServiceProductInfoViewV2>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (ReadWebServiceProductInfoViewV2) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(ReadWebServiceProductInfoViewV2)));
        }

        /// <summary>
        /// GET PRODUCT GetProductView By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        public List<ReadWebServiceBulkProductFetchResponse> ReadWebServiceProductServiceGetProductViewsByLocationV1JSON (ReadWebServiceBulkProductFetchContext bulkProductFetchContext)
        {
             ApiResponse<List<ReadWebServiceBulkProductFetchResponse>> localVarResponse = ReadWebServiceProductServiceGetProductViewsByLocationV1JSONWithHttpInfo(bulkProductFetchContext);
             return localVarResponse.Data;
        }

        /// <summary>
        /// GET PRODUCT GetProductView By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>ApiResponse of List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        public ApiResponse< List<ReadWebServiceBulkProductFetchResponse> > ReadWebServiceProductServiceGetProductViewsByLocationV1JSONWithHttpInfo (ReadWebServiceBulkProductFetchContext bulkProductFetchContext)
        {
            // verify the required parameter 'bulkProductFetchContext' is set
            if (bulkProductFetchContext == null)
                throw new ApiException(400, "Missing required parameter 'bulkProductFetchContext' when calling DefaultApi->ReadWebServiceProductServiceGetProductViewsByLocationV1JSON");

            var localVarPath = "/GetProductViewByLocationV1JSON";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json"
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            if (bulkProductFetchContext != null && bulkProductFetchContext.GetType() != typeof(byte[]))
            {
                localVarPostBody = this.Configuration.ApiClient.Serialize(bulkProductFetchContext); // http body (model) parameter
            }
            else
            {
                localVarPostBody = bulkProductFetchContext; // byte array
            }


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) this.Configuration.ApiClient.CallApi(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ReadWebServiceProductServiceGetProductViewsByLocationV1JSON", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<List<ReadWebServiceBulkProductFetchResponse>>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (List<ReadWebServiceBulkProductFetchResponse>) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(List<ReadWebServiceBulkProductFetchResponse>)));
        }

        /// <summary>
        /// GET PRODUCT GetProductView By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of List&lt;ReadWebServiceBulkProductFetchResponse&gt;</returns>
        public async System.Threading.Tasks.Task<List<ReadWebServiceBulkProductFetchResponse>> ReadWebServiceProductServiceGetProductViewsByLocationV1JSONAsync (ReadWebServiceBulkProductFetchContext bulkProductFetchContext)
        {
             ApiResponse<List<ReadWebServiceBulkProductFetchResponse>> localVarResponse = await ReadWebServiceProductServiceGetProductViewsByLocationV1JSONAsyncWithHttpInfo(bulkProductFetchContext);
             return localVarResponse.Data;

        }

        /// <summary>
        /// GET PRODUCT GetProductView By Location 
        /// </summary>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="bulkProductFetchContext"></param>
        /// <returns>Task of ApiResponse (List&lt;ReadWebServiceBulkProductFetchResponse&gt;)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<List<ReadWebServiceBulkProductFetchResponse>>> ReadWebServiceProductServiceGetProductViewsByLocationV1JSONAsyncWithHttpInfo (ReadWebServiceBulkProductFetchContext bulkProductFetchContext)
        {
            // verify the required parameter 'bulkProductFetchContext' is set
            if (bulkProductFetchContext == null)
                throw new ApiException(400, "Missing required parameter 'bulkProductFetchContext' when calling DefaultApi->ReadWebServiceProductServiceGetProductViewsByLocationV1JSON");

            var localVarPath = "/GetProductViewByLocationV1JSON";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(this.Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json"
            };
            String localVarHttpContentType = this.Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = this.Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            if (bulkProductFetchContext != null && bulkProductFetchContext.GetType() != typeof(byte[]))
            {
                localVarPostBody = this.Configuration.ApiClient.Serialize(bulkProductFetchContext); // http body (model) parameter
            }
            else
            {
                localVarPostBody = bulkProductFetchContext; // byte array
            }


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await this.Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.POST, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("ReadWebServiceProductServiceGetProductViewsByLocationV1JSON", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<List<ReadWebServiceBulkProductFetchResponse>>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (List<ReadWebServiceBulkProductFetchResponse>) this.Configuration.ApiClient.Deserialize(localVarResponse, typeof(List<ReadWebServiceBulkProductFetchResponse>)));
        }

    }
}
