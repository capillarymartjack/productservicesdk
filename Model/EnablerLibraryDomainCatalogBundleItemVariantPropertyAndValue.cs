/* 
 * ReadWebService
 *
 * RWS REST Endpoint
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue
    /// </summary>
    [DataContract]
    public partial class EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue :  IEquatable<EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue" /> class.
        /// </summary>
        /// <param name="bundlePropertyId">bundlePropertyId.</param>
        /// <param name="bundleItemId">bundleItemId.</param>
        /// <param name="variantPropertyId">variantPropertyId.</param>
        /// <param name="variantValueId">variantValueId.</param>
        /// <param name="navBundleItem">navBundleItem.</param>
        /// <param name="isRestricted">isRestricted.</param>
        /// <param name="variantProductId">variantProductId.</param>
        /// <param name="isIncludedInBundlePrice">isIncludedInBundlePrice.</param>
        /// <param name="isDefault">isDefault.</param>
        /// <param name="overridedPrice">overridedPrice.</param>
        /// <param name="isOverridedPrice">isOverridedPrice.</param>
        /// <param name="variantLevelMaxQty">variantLevelMaxQty.</param>
        /// <param name="bundleProductId">bundleProductId.</param>
        public EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue(long? bundlePropertyId = default(long?), long? bundleItemId = default(long?), long? variantPropertyId = default(long?), long? variantValueId = default(long?), EnablerLibraryDomainCatalogBundleItem navBundleItem = default(EnablerLibraryDomainCatalogBundleItem), bool? isRestricted = default(bool?), long? variantProductId = default(long?), bool? isIncludedInBundlePrice = default(bool?), bool? isDefault = default(bool?), double? overridedPrice = default(double?), bool? isOverridedPrice = default(bool?), int? variantLevelMaxQty = default(int?), long? bundleProductId = default(long?))
        {
            this.BundlePropertyId = bundlePropertyId;
            this.BundleItemId = bundleItemId;
            this.VariantPropertyId = variantPropertyId;
            this.VariantValueId = variantValueId;
            this.NavBundleItem = navBundleItem;
            this.IsRestricted = isRestricted;
            this.VariantProductId = variantProductId;
            this.IsIncludedInBundlePrice = isIncludedInBundlePrice;
            this.IsDefault = isDefault;
            this.OverridedPrice = overridedPrice;
            this.IsOverridedPrice = isOverridedPrice;
            this.VariantLevelMaxQty = variantLevelMaxQty;
            this.BundleProductId = bundleProductId;
        }
        
        /// <summary>
        /// Gets or Sets BundlePropertyId
        /// </summary>
        [DataMember(Name="BundlePropertyId", EmitDefaultValue=false)]
        public long? BundlePropertyId { get; set; }

        /// <summary>
        /// Gets or Sets BundleItemId
        /// </summary>
        [DataMember(Name="BundleItemId", EmitDefaultValue=false)]
        public long? BundleItemId { get; set; }

        /// <summary>
        /// Gets or Sets VariantPropertyId
        /// </summary>
        [DataMember(Name="VariantPropertyId", EmitDefaultValue=false)]
        public long? VariantPropertyId { get; set; }

        /// <summary>
        /// Gets or Sets VariantValueId
        /// </summary>
        [DataMember(Name="VariantValueId", EmitDefaultValue=false)]
        public long? VariantValueId { get; set; }

        /// <summary>
        /// Gets or Sets NavBundleItem
        /// </summary>
        [DataMember(Name="NavBundleItem", EmitDefaultValue=false)]
        public EnablerLibraryDomainCatalogBundleItem NavBundleItem { get; set; }

        /// <summary>
        /// Gets or Sets IsRestricted
        /// </summary>
        [DataMember(Name="IsRestricted", EmitDefaultValue=false)]
        public bool? IsRestricted { get; set; }

        /// <summary>
        /// Gets or Sets VariantProductId
        /// </summary>
        [DataMember(Name="VariantProductId", EmitDefaultValue=false)]
        public long? VariantProductId { get; set; }

        /// <summary>
        /// Gets or Sets IsIncludedInBundlePrice
        /// </summary>
        [DataMember(Name="IsIncludedInBundlePrice", EmitDefaultValue=false)]
        public bool? IsIncludedInBundlePrice { get; set; }

        /// <summary>
        /// Gets or Sets IsDefault
        /// </summary>
        [DataMember(Name="IsDefault", EmitDefaultValue=false)]
        public bool? IsDefault { get; set; }

        /// <summary>
        /// Gets or Sets OverridedPrice
        /// </summary>
        [DataMember(Name="OverridedPrice", EmitDefaultValue=false)]
        public double? OverridedPrice { get; set; }

        /// <summary>
        /// Gets or Sets IsOverridedPrice
        /// </summary>
        [DataMember(Name="IsOverridedPrice", EmitDefaultValue=false)]
        public bool? IsOverridedPrice { get; set; }

        /// <summary>
        /// Gets or Sets VariantLevelMaxQty
        /// </summary>
        [DataMember(Name="VariantLevelMaxQty", EmitDefaultValue=false)]
        public int? VariantLevelMaxQty { get; set; }

        /// <summary>
        /// Gets or Sets BundleProductId
        /// </summary>
        [DataMember(Name="BundleProductId", EmitDefaultValue=false)]
        public long? BundleProductId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue {\n");
            sb.Append("  BundlePropertyId: ").Append(BundlePropertyId).Append("\n");
            sb.Append("  BundleItemId: ").Append(BundleItemId).Append("\n");
            sb.Append("  VariantPropertyId: ").Append(VariantPropertyId).Append("\n");
            sb.Append("  VariantValueId: ").Append(VariantValueId).Append("\n");
            sb.Append("  NavBundleItem: ").Append(NavBundleItem).Append("\n");
            sb.Append("  IsRestricted: ").Append(IsRestricted).Append("\n");
            sb.Append("  VariantProductId: ").Append(VariantProductId).Append("\n");
            sb.Append("  IsIncludedInBundlePrice: ").Append(IsIncludedInBundlePrice).Append("\n");
            sb.Append("  IsDefault: ").Append(IsDefault).Append("\n");
            sb.Append("  OverridedPrice: ").Append(OverridedPrice).Append("\n");
            sb.Append("  IsOverridedPrice: ").Append(IsOverridedPrice).Append("\n");
            sb.Append("  VariantLevelMaxQty: ").Append(VariantLevelMaxQty).Append("\n");
            sb.Append("  BundleProductId: ").Append(BundleProductId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue);
        }

        /// <summary>
        /// Returns true if EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue instances are equal
        /// </summary>
        /// <param name="input">Instance of EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(EnablerLibraryDomainCatalogBundleItemVariantPropertyAndValue input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BundlePropertyId == input.BundlePropertyId ||
                    (this.BundlePropertyId != null &&
                    this.BundlePropertyId.Equals(input.BundlePropertyId))
                ) && 
                (
                    this.BundleItemId == input.BundleItemId ||
                    (this.BundleItemId != null &&
                    this.BundleItemId.Equals(input.BundleItemId))
                ) && 
                (
                    this.VariantPropertyId == input.VariantPropertyId ||
                    (this.VariantPropertyId != null &&
                    this.VariantPropertyId.Equals(input.VariantPropertyId))
                ) && 
                (
                    this.VariantValueId == input.VariantValueId ||
                    (this.VariantValueId != null &&
                    this.VariantValueId.Equals(input.VariantValueId))
                ) && 
                (
                    this.NavBundleItem == input.NavBundleItem ||
                    (this.NavBundleItem != null &&
                    this.NavBundleItem.Equals(input.NavBundleItem))
                ) && 
                (
                    this.IsRestricted == input.IsRestricted ||
                    (this.IsRestricted != null &&
                    this.IsRestricted.Equals(input.IsRestricted))
                ) && 
                (
                    this.VariantProductId == input.VariantProductId ||
                    (this.VariantProductId != null &&
                    this.VariantProductId.Equals(input.VariantProductId))
                ) && 
                (
                    this.IsIncludedInBundlePrice == input.IsIncludedInBundlePrice ||
                    (this.IsIncludedInBundlePrice != null &&
                    this.IsIncludedInBundlePrice.Equals(input.IsIncludedInBundlePrice))
                ) && 
                (
                    this.IsDefault == input.IsDefault ||
                    (this.IsDefault != null &&
                    this.IsDefault.Equals(input.IsDefault))
                ) && 
                (
                    this.OverridedPrice == input.OverridedPrice ||
                    (this.OverridedPrice != null &&
                    this.OverridedPrice.Equals(input.OverridedPrice))
                ) && 
                (
                    this.IsOverridedPrice == input.IsOverridedPrice ||
                    (this.IsOverridedPrice != null &&
                    this.IsOverridedPrice.Equals(input.IsOverridedPrice))
                ) && 
                (
                    this.VariantLevelMaxQty == input.VariantLevelMaxQty ||
                    (this.VariantLevelMaxQty != null &&
                    this.VariantLevelMaxQty.Equals(input.VariantLevelMaxQty))
                ) && 
                (
                    this.BundleProductId == input.BundleProductId ||
                    (this.BundleProductId != null &&
                    this.BundleProductId.Equals(input.BundleProductId))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.BundlePropertyId != null)
                    hashCode = hashCode * 59 + this.BundlePropertyId.GetHashCode();
                if (this.BundleItemId != null)
                    hashCode = hashCode * 59 + this.BundleItemId.GetHashCode();
                if (this.VariantPropertyId != null)
                    hashCode = hashCode * 59 + this.VariantPropertyId.GetHashCode();
                if (this.VariantValueId != null)
                    hashCode = hashCode * 59 + this.VariantValueId.GetHashCode();
                if (this.NavBundleItem != null)
                    hashCode = hashCode * 59 + this.NavBundleItem.GetHashCode();
                if (this.IsRestricted != null)
                    hashCode = hashCode * 59 + this.IsRestricted.GetHashCode();
                if (this.VariantProductId != null)
                    hashCode = hashCode * 59 + this.VariantProductId.GetHashCode();
                if (this.IsIncludedInBundlePrice != null)
                    hashCode = hashCode * 59 + this.IsIncludedInBundlePrice.GetHashCode();
                if (this.IsDefault != null)
                    hashCode = hashCode * 59 + this.IsDefault.GetHashCode();
                if (this.OverridedPrice != null)
                    hashCode = hashCode * 59 + this.OverridedPrice.GetHashCode();
                if (this.IsOverridedPrice != null)
                    hashCode = hashCode * 59 + this.IsOverridedPrice.GetHashCode();
                if (this.VariantLevelMaxQty != null)
                    hashCode = hashCode * 59 + this.VariantLevelMaxQty.GetHashCode();
                if (this.BundleProductId != null)
                    hashCode = hashCode * 59 + this.BundleProductId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
