/* 
 * ReadWebService
 *
 * RWS REST Endpoint
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// EnablerLibraryDomainCatalogBundleProduct
    /// </summary>
    [DataContract]
    public partial class EnablerLibraryDomainCatalogBundleProduct :  IEquatable<EnablerLibraryDomainCatalogBundleProduct>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnablerLibraryDomainCatalogBundleProduct" /> class.
        /// </summary>
        /// <param name="bundleProductID">bundleProductID.</param>
        /// <param name="productId">productId.</param>
        /// <param name="suggestivePercent">suggestivePercent.</param>
        /// <param name="priceDisplayOption">priceDisplayOption.</param>
        /// <param name="isDynamicPrice">isDynamicPrice.</param>
        /// <param name="isDynamicWeight">isDynamicWeight.</param>
        /// <param name="isIndividualDispatch">isIndividualDispatch.</param>
        /// <param name="merchantId">merchantId.</param>
        /// <param name="bundleProductGroups">bundleProductGroups.</param>
        public EnablerLibraryDomainCatalogBundleProduct(long? bundleProductID = default(long?), long? productId = default(long?), long? suggestivePercent = default(long?), string priceDisplayOption = default(string), bool? isDynamicPrice = default(bool?), bool? isDynamicWeight = default(bool?), bool? isIndividualDispatch = default(bool?), string merchantId = default(string), List<EnablerLibraryDomainCatalogBundleGroup> bundleProductGroups = default(List<EnablerLibraryDomainCatalogBundleGroup>))
        {
            this.BundleProductID = bundleProductID;
            this.ProductId = productId;
            this.SuggestivePercent = suggestivePercent;
            this.PriceDisplayOption = priceDisplayOption;
            this.IsDynamicPrice = isDynamicPrice;
            this.IsDynamicWeight = isDynamicWeight;
            this.IsIndividualDispatch = isIndividualDispatch;
            this.MerchantId = merchantId;
            this.BundleProductGroups = bundleProductGroups;
        }
        
        /// <summary>
        /// Gets or Sets BundleProductID
        /// </summary>
        [DataMember(Name="BundleProductID", EmitDefaultValue=false)]
        public long? BundleProductID { get; set; }

        /// <summary>
        /// Gets or Sets ProductId
        /// </summary>
        [DataMember(Name="ProductId", EmitDefaultValue=false)]
        public long? ProductId { get; set; }

        /// <summary>
        /// Gets or Sets SuggestivePercent
        /// </summary>
        [DataMember(Name="SuggestivePercent", EmitDefaultValue=false)]
        public long? SuggestivePercent { get; set; }

        /// <summary>
        /// Gets or Sets PriceDisplayOption
        /// </summary>
        [DataMember(Name="PriceDisplayOption", EmitDefaultValue=false)]
        public string PriceDisplayOption { get; set; }

        /// <summary>
        /// Gets or Sets IsDynamicPrice
        /// </summary>
        [DataMember(Name="IsDynamicPrice", EmitDefaultValue=false)]
        public bool? IsDynamicPrice { get; set; }

        /// <summary>
        /// Gets or Sets IsDynamicWeight
        /// </summary>
        [DataMember(Name="IsDynamicWeight", EmitDefaultValue=false)]
        public bool? IsDynamicWeight { get; set; }

        /// <summary>
        /// Gets or Sets IsIndividualDispatch
        /// </summary>
        [DataMember(Name="IsIndividualDispatch", EmitDefaultValue=false)]
        public bool? IsIndividualDispatch { get; set; }

        /// <summary>
        /// Gets or Sets MerchantId
        /// </summary>
        [DataMember(Name="MerchantId", EmitDefaultValue=false)]
        public string MerchantId { get; set; }

        /// <summary>
        /// Gets or Sets BundleProductGroups
        /// </summary>
        [DataMember(Name="BundleProductGroups", EmitDefaultValue=false)]
        public List<EnablerLibraryDomainCatalogBundleGroup> BundleProductGroups { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EnablerLibraryDomainCatalogBundleProduct {\n");
            sb.Append("  BundleProductID: ").Append(BundleProductID).Append("\n");
            sb.Append("  ProductId: ").Append(ProductId).Append("\n");
            sb.Append("  SuggestivePercent: ").Append(SuggestivePercent).Append("\n");
            sb.Append("  PriceDisplayOption: ").Append(PriceDisplayOption).Append("\n");
            sb.Append("  IsDynamicPrice: ").Append(IsDynamicPrice).Append("\n");
            sb.Append("  IsDynamicWeight: ").Append(IsDynamicWeight).Append("\n");
            sb.Append("  IsIndividualDispatch: ").Append(IsIndividualDispatch).Append("\n");
            sb.Append("  MerchantId: ").Append(MerchantId).Append("\n");
            sb.Append("  BundleProductGroups: ").Append(BundleProductGroups).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as EnablerLibraryDomainCatalogBundleProduct);
        }

        /// <summary>
        /// Returns true if EnablerLibraryDomainCatalogBundleProduct instances are equal
        /// </summary>
        /// <param name="input">Instance of EnablerLibraryDomainCatalogBundleProduct to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(EnablerLibraryDomainCatalogBundleProduct input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BundleProductID == input.BundleProductID ||
                    (this.BundleProductID != null &&
                    this.BundleProductID.Equals(input.BundleProductID))
                ) && 
                (
                    this.ProductId == input.ProductId ||
                    (this.ProductId != null &&
                    this.ProductId.Equals(input.ProductId))
                ) && 
                (
                    this.SuggestivePercent == input.SuggestivePercent ||
                    (this.SuggestivePercent != null &&
                    this.SuggestivePercent.Equals(input.SuggestivePercent))
                ) && 
                (
                    this.PriceDisplayOption == input.PriceDisplayOption ||
                    (this.PriceDisplayOption != null &&
                    this.PriceDisplayOption.Equals(input.PriceDisplayOption))
                ) && 
                (
                    this.IsDynamicPrice == input.IsDynamicPrice ||
                    (this.IsDynamicPrice != null &&
                    this.IsDynamicPrice.Equals(input.IsDynamicPrice))
                ) && 
                (
                    this.IsDynamicWeight == input.IsDynamicWeight ||
                    (this.IsDynamicWeight != null &&
                    this.IsDynamicWeight.Equals(input.IsDynamicWeight))
                ) && 
                (
                    this.IsIndividualDispatch == input.IsIndividualDispatch ||
                    (this.IsIndividualDispatch != null &&
                    this.IsIndividualDispatch.Equals(input.IsIndividualDispatch))
                ) && 
                (
                    this.MerchantId == input.MerchantId ||
                    (this.MerchantId != null &&
                    this.MerchantId.Equals(input.MerchantId))
                ) && 
                (
                    this.BundleProductGroups == input.BundleProductGroups ||
                    this.BundleProductGroups != null &&
                    this.BundleProductGroups.SequenceEqual(input.BundleProductGroups)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.BundleProductID != null)
                    hashCode = hashCode * 59 + this.BundleProductID.GetHashCode();
                if (this.ProductId != null)
                    hashCode = hashCode * 59 + this.ProductId.GetHashCode();
                if (this.SuggestivePercent != null)
                    hashCode = hashCode * 59 + this.SuggestivePercent.GetHashCode();
                if (this.PriceDisplayOption != null)
                    hashCode = hashCode * 59 + this.PriceDisplayOption.GetHashCode();
                if (this.IsDynamicPrice != null)
                    hashCode = hashCode * 59 + this.IsDynamicPrice.GetHashCode();
                if (this.IsDynamicWeight != null)
                    hashCode = hashCode * 59 + this.IsDynamicWeight.GetHashCode();
                if (this.IsIndividualDispatch != null)
                    hashCode = hashCode * 59 + this.IsIndividualDispatch.GetHashCode();
                if (this.MerchantId != null)
                    hashCode = hashCode * 59 + this.MerchantId.GetHashCode();
                if (this.BundleProductGroups != null)
                    hashCode = hashCode * 59 + this.BundleProductGroups.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
