/* 
 * ReadWebService
 *
 * RWS REST Endpoint
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// ReadWebServiceVariantInfoV2
    /// </summary>
    [DataContract]
    public partial class ReadWebServiceVariantInfoV2 :  IEquatable<ReadWebServiceVariantInfoV2>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWebServiceVariantInfoV2" /> class.
        /// </summary>
        /// <param name="webPrice">webPrice.</param>
        /// <param name="buyingWeight">buyingWeight.</param>
        /// <param name="dicVariantPropertyIdValueId">dicVariantPropertyIdValueId.</param>
        /// <param name="dicVariantProperty">dicVariantProperty.</param>
        /// <param name="id">id.</param>
        /// <param name="productId">productId.</param>
        /// <param name="sku">sku.</param>
        /// <param name="merchantId">merchantId.</param>
        /// <param name="availability">availability.</param>
        /// <param name="inventory">inventory.</param>
        /// <param name="weight">weight.</param>
        /// <param name="barcode">barcode.</param>
        /// <param name="mRP">mRP.</param>
        public ReadWebServiceVariantInfoV2(decimal? webPrice = default(decimal?), decimal? buyingWeight = default(decimal?), List<ReadWebServiceDicPropertyValueIdV2> dicVariantPropertyIdValueId = default(List<ReadWebServiceDicPropertyValueIdV2>), List<ReadWebServiceDicPropertyIdV2> dicVariantProperty = default(List<ReadWebServiceDicPropertyIdV2>), string id = default(string), long? productId = default(long?), string sku = default(string), string merchantId = default(string), bool? availability = default(bool?), int? inventory = default(int?), decimal? weight = default(decimal?), string barcode = default(string), decimal? mRP = default(decimal?))
        {
            this.WebPrice = webPrice;
            this.BuyingWeight = buyingWeight;
            this.DicVariantPropertyIdValueId = dicVariantPropertyIdValueId;
            this.DicVariantProperty = dicVariantProperty;
            this.Id = id;
            this.ProductId = productId;
            this.Sku = sku;
            this.MerchantId = merchantId;
            this.Availability = availability;
            this.Inventory = inventory;
            this.Weight = weight;
            this.Barcode = barcode;
            this.MRP = mRP;
        }
        
        /// <summary>
        /// Gets or Sets WebPrice
        /// </summary>
        [DataMember(Name="WebPrice", EmitDefaultValue=false)]
        public decimal? WebPrice { get; set; }

        /// <summary>
        /// Gets or Sets BuyingWeight
        /// </summary>
        [DataMember(Name="BuyingWeight", EmitDefaultValue=false)]
        public decimal? BuyingWeight { get; set; }

        /// <summary>
        /// Gets or Sets DicVariantPropertyIdValueId
        /// </summary>
        [DataMember(Name="DicVariantPropertyIdValueId", EmitDefaultValue=false)]
        public List<ReadWebServiceDicPropertyValueIdV2> DicVariantPropertyIdValueId { get; set; }

        /// <summary>
        /// Gets or Sets DicVariantProperty
        /// </summary>
        [DataMember(Name="DicVariantProperty", EmitDefaultValue=false)]
        public List<ReadWebServiceDicPropertyIdV2> DicVariantProperty { get; set; }

        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="Id", EmitDefaultValue=false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets ProductId
        /// </summary>
        [DataMember(Name="ProductId", EmitDefaultValue=false)]
        public long? ProductId { get; set; }

        /// <summary>
        /// Gets or Sets Sku
        /// </summary>
        [DataMember(Name="Sku", EmitDefaultValue=false)]
        public string Sku { get; set; }

        /// <summary>
        /// Gets or Sets MerchantId
        /// </summary>
        [DataMember(Name="MerchantId", EmitDefaultValue=false)]
        public string MerchantId { get; set; }

        /// <summary>
        /// Gets or Sets Availability
        /// </summary>
        [DataMember(Name="Availability", EmitDefaultValue=false)]
        public bool? Availability { get; set; }

        /// <summary>
        /// Gets or Sets Inventory
        /// </summary>
        [DataMember(Name="Inventory", EmitDefaultValue=false)]
        public int? Inventory { get; set; }

        /// <summary>
        /// Gets or Sets Weight
        /// </summary>
        [DataMember(Name="Weight", EmitDefaultValue=false)]
        public decimal? Weight { get; set; }

        /// <summary>
        /// Gets or Sets Barcode
        /// </summary>
        [DataMember(Name="Barcode", EmitDefaultValue=false)]
        public string Barcode { get; set; }

        /// <summary>
        /// Gets or Sets MRP
        /// </summary>
        [DataMember(Name="MRP", EmitDefaultValue=false)]
        public decimal? MRP { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ReadWebServiceVariantInfoV2 {\n");
            sb.Append("  WebPrice: ").Append(WebPrice).Append("\n");
            sb.Append("  BuyingWeight: ").Append(BuyingWeight).Append("\n");
            sb.Append("  DicVariantPropertyIdValueId: ").Append(DicVariantPropertyIdValueId).Append("\n");
            sb.Append("  DicVariantProperty: ").Append(DicVariantProperty).Append("\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  ProductId: ").Append(ProductId).Append("\n");
            sb.Append("  Sku: ").Append(Sku).Append("\n");
            sb.Append("  MerchantId: ").Append(MerchantId).Append("\n");
            sb.Append("  Availability: ").Append(Availability).Append("\n");
            sb.Append("  Inventory: ").Append(Inventory).Append("\n");
            sb.Append("  Weight: ").Append(Weight).Append("\n");
            sb.Append("  Barcode: ").Append(Barcode).Append("\n");
            sb.Append("  MRP: ").Append(MRP).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ReadWebServiceVariantInfoV2);
        }

        /// <summary>
        /// Returns true if ReadWebServiceVariantInfoV2 instances are equal
        /// </summary>
        /// <param name="input">Instance of ReadWebServiceVariantInfoV2 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ReadWebServiceVariantInfoV2 input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.WebPrice == input.WebPrice ||
                    (this.WebPrice != null &&
                    this.WebPrice.Equals(input.WebPrice))
                ) && 
                (
                    this.BuyingWeight == input.BuyingWeight ||
                    (this.BuyingWeight != null &&
                    this.BuyingWeight.Equals(input.BuyingWeight))
                ) && 
                (
                    this.DicVariantPropertyIdValueId == input.DicVariantPropertyIdValueId ||
                    this.DicVariantPropertyIdValueId != null &&
                    this.DicVariantPropertyIdValueId.SequenceEqual(input.DicVariantPropertyIdValueId)
                ) && 
                (
                    this.DicVariantProperty == input.DicVariantProperty ||
                    this.DicVariantProperty != null &&
                    this.DicVariantProperty.SequenceEqual(input.DicVariantProperty)
                ) && 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.ProductId == input.ProductId ||
                    (this.ProductId != null &&
                    this.ProductId.Equals(input.ProductId))
                ) && 
                (
                    this.Sku == input.Sku ||
                    (this.Sku != null &&
                    this.Sku.Equals(input.Sku))
                ) && 
                (
                    this.MerchantId == input.MerchantId ||
                    (this.MerchantId != null &&
                    this.MerchantId.Equals(input.MerchantId))
                ) && 
                (
                    this.Availability == input.Availability ||
                    (this.Availability != null &&
                    this.Availability.Equals(input.Availability))
                ) && 
                (
                    this.Inventory == input.Inventory ||
                    (this.Inventory != null &&
                    this.Inventory.Equals(input.Inventory))
                ) && 
                (
                    this.Weight == input.Weight ||
                    (this.Weight != null &&
                    this.Weight.Equals(input.Weight))
                ) && 
                (
                    this.Barcode == input.Barcode ||
                    (this.Barcode != null &&
                    this.Barcode.Equals(input.Barcode))
                ) && 
                (
                    this.MRP == input.MRP ||
                    (this.MRP != null &&
                    this.MRP.Equals(input.MRP))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.WebPrice != null)
                    hashCode = hashCode * 59 + this.WebPrice.GetHashCode();
                if (this.BuyingWeight != null)
                    hashCode = hashCode * 59 + this.BuyingWeight.GetHashCode();
                if (this.DicVariantPropertyIdValueId != null)
                    hashCode = hashCode * 59 + this.DicVariantPropertyIdValueId.GetHashCode();
                if (this.DicVariantProperty != null)
                    hashCode = hashCode * 59 + this.DicVariantProperty.GetHashCode();
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                if (this.ProductId != null)
                    hashCode = hashCode * 59 + this.ProductId.GetHashCode();
                if (this.Sku != null)
                    hashCode = hashCode * 59 + this.Sku.GetHashCode();
                if (this.MerchantId != null)
                    hashCode = hashCode * 59 + this.MerchantId.GetHashCode();
                if (this.Availability != null)
                    hashCode = hashCode * 59 + this.Availability.GetHashCode();
                if (this.Inventory != null)
                    hashCode = hashCode * 59 + this.Inventory.GetHashCode();
                if (this.Weight != null)
                    hashCode = hashCode * 59 + this.Weight.GetHashCode();
                if (this.Barcode != null)
                    hashCode = hashCode * 59 + this.Barcode.GetHashCode();
                if (this.MRP != null)
                    hashCode = hashCode * 59 + this.MRP.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
