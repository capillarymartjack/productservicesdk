/* 
 * ReadWebService
 *
 * RWS REST Endpoint
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// ReadWebServiceBundleProductFetchContext
    /// </summary>
    [DataContract]
    public partial class ReadWebServiceBundleProductFetchContext :  IEquatable<ReadWebServiceBundleProductFetchContext>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWebServiceBundleProductFetchContext" /> class.
        /// </summary>
        /// <param name="merchantId">merchantId.</param>
        /// <param name="productId">productId.</param>
        /// <param name="variantProductId">variantProductId.</param>
        /// <param name="quantity">quantity.</param>
        /// <param name="bundleGroupId">bundleGroupId.</param>
        /// <param name="loadDefaultItems">loadDefaultItems.</param>
        /// <param name="userId">userId.</param>
        /// <param name="locationId">locationId.</param>
        public ReadWebServiceBundleProductFetchContext(string merchantId = default(string), long? productId = default(long?), long? variantProductId = default(long?), double? quantity = default(double?), long? bundleGroupId = default(long?), bool? loadDefaultItems = default(bool?), string userId = default(string), long? locationId = default(long?))
        {
            this.MerchantId = merchantId;
            this.ProductId = productId;
            this.VariantProductId = variantProductId;
            this.Quantity = quantity;
            this.BundleGroupId = bundleGroupId;
            this.LoadDefaultItems = loadDefaultItems;
            this.UserId = userId;
            this.LocationId = locationId;
        }
        
        /// <summary>
        /// Gets or Sets MerchantId
        /// </summary>
        [DataMember(Name="MerchantId", EmitDefaultValue=false)]
        public string MerchantId { get; set; }

        /// <summary>
        /// Gets or Sets ProductId
        /// </summary>
        [DataMember(Name="ProductId", EmitDefaultValue=false)]
        public long? ProductId { get; set; }

        /// <summary>
        /// Gets or Sets VariantProductId
        /// </summary>
        [DataMember(Name="VariantProductId", EmitDefaultValue=false)]
        public long? VariantProductId { get; set; }

        /// <summary>
        /// Gets or Sets Quantity
        /// </summary>
        [DataMember(Name="Quantity", EmitDefaultValue=false)]
        public double? Quantity { get; set; }

        /// <summary>
        /// Gets or Sets BundleGroupId
        /// </summary>
        [DataMember(Name="BundleGroupId", EmitDefaultValue=false)]
        public long? BundleGroupId { get; set; }

        /// <summary>
        /// Gets or Sets LoadDefaultItems
        /// </summary>
        [DataMember(Name="LoadDefaultItems", EmitDefaultValue=false)]
        public bool? LoadDefaultItems { get; set; }

        /// <summary>
        /// Gets or Sets UserId
        /// </summary>
        [DataMember(Name="UserId", EmitDefaultValue=false)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or Sets LocationId
        /// </summary>
        [DataMember(Name="LocationId", EmitDefaultValue=false)]
        public long? LocationId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ReadWebServiceBundleProductFetchContext {\n");
            sb.Append("  MerchantId: ").Append(MerchantId).Append("\n");
            sb.Append("  ProductId: ").Append(ProductId).Append("\n");
            sb.Append("  VariantProductId: ").Append(VariantProductId).Append("\n");
            sb.Append("  Quantity: ").Append(Quantity).Append("\n");
            sb.Append("  BundleGroupId: ").Append(BundleGroupId).Append("\n");
            sb.Append("  LoadDefaultItems: ").Append(LoadDefaultItems).Append("\n");
            sb.Append("  UserId: ").Append(UserId).Append("\n");
            sb.Append("  LocationId: ").Append(LocationId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ReadWebServiceBundleProductFetchContext);
        }

        /// <summary>
        /// Returns true if ReadWebServiceBundleProductFetchContext instances are equal
        /// </summary>
        /// <param name="input">Instance of ReadWebServiceBundleProductFetchContext to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ReadWebServiceBundleProductFetchContext input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.MerchantId == input.MerchantId ||
                    (this.MerchantId != null &&
                    this.MerchantId.Equals(input.MerchantId))
                ) && 
                (
                    this.ProductId == input.ProductId ||
                    (this.ProductId != null &&
                    this.ProductId.Equals(input.ProductId))
                ) && 
                (
                    this.VariantProductId == input.VariantProductId ||
                    (this.VariantProductId != null &&
                    this.VariantProductId.Equals(input.VariantProductId))
                ) && 
                (
                    this.Quantity == input.Quantity ||
                    (this.Quantity != null &&
                    this.Quantity.Equals(input.Quantity))
                ) && 
                (
                    this.BundleGroupId == input.BundleGroupId ||
                    (this.BundleGroupId != null &&
                    this.BundleGroupId.Equals(input.BundleGroupId))
                ) && 
                (
                    this.LoadDefaultItems == input.LoadDefaultItems ||
                    (this.LoadDefaultItems != null &&
                    this.LoadDefaultItems.Equals(input.LoadDefaultItems))
                ) && 
                (
                    this.UserId == input.UserId ||
                    (this.UserId != null &&
                    this.UserId.Equals(input.UserId))
                ) && 
                (
                    this.LocationId == input.LocationId ||
                    (this.LocationId != null &&
                    this.LocationId.Equals(input.LocationId))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.MerchantId != null)
                    hashCode = hashCode * 59 + this.MerchantId.GetHashCode();
                if (this.ProductId != null)
                    hashCode = hashCode * 59 + this.ProductId.GetHashCode();
                if (this.VariantProductId != null)
                    hashCode = hashCode * 59 + this.VariantProductId.GetHashCode();
                if (this.Quantity != null)
                    hashCode = hashCode * 59 + this.Quantity.GetHashCode();
                if (this.BundleGroupId != null)
                    hashCode = hashCode * 59 + this.BundleGroupId.GetHashCode();
                if (this.LoadDefaultItems != null)
                    hashCode = hashCode * 59 + this.LoadDefaultItems.GetHashCode();
                if (this.UserId != null)
                    hashCode = hashCode * 59 + this.UserId.GetHashCode();
                if (this.LocationId != null)
                    hashCode = hashCode * 59 + this.LocationId.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
